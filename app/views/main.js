module.exports = (user) => {
  return `<div class="column" id="left-column">
		<div class="top-left">Stack Overflow</div>
		<div class="bottom">
		  <div class="profile-avatar">
		    <img id="profile-image" src="${user._profile_image}" alt="${user._display_name}">
      </div>
			<ul class="nav">
				<li>Profile</li>
				<li>Questions</li>
				<li>Tags</li>
			</ul>
		</div>
	</div>

	<div class="column" id="right-column">
		<div class="top-right">lol</div>
		<div class="bottom">lol</div>
	</div>`.compact()
}