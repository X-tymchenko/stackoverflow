const {BrowserWindow} = require('electron')

module.exports = class LoginWindow extends BrowserWindow {
  constructor (params, callback) {
    super(params)
    this.callback = callback
    this.webContents.on('did-get-redirect-request', this.DidRedirect.bind(this))
    this.webContents.on('did-finish-load',
      this.showAuthWindowIfNotLoggedIn.bind(this))
  }

  showAuthWindowIfNotLoggedIn () {
    this.show()
  }

  unloadAndCloseAuthWindow (token, expires) {
    this.webContents.removeListener('did-finish-load',
      this.showAuthWindowIfNotLoggedIn)
    this.callback(token, expires, new Date())
    this.destroy()
  }

  loadAuthUrl () {
    this.loadURL(
      'https://stackexchange.com/oauth/dialog?redirect_uri=https://stackexchange.com/oauth/login_success&client_id=10932&scope=write_access private_info read_inbox')
  }

  DidRedirect (event, oldUrl, newUrl) {
    const isMainPage = !/[a-zA-Z]+/.test(
      newUrl.replace(/(https|http)/, '').replace('//stackexchange.com', ''))
    if (isMainPage) {
      return this.loadAuthUrl()
    }
    const isError = newUrl.indexOf('error') >= 0
    const hasToken = newUrl.indexOf('access_token') >= 0
    if (isError || !hasToken) {
      return
    }
    // Success authentication
    const hashPosition = newUrl.indexOf('#') + 1
    let [token, expires] = newUrl.substring(hashPosition).split('&')

    token = token.split('=')[1]
    expires = expires.split('=')[1]
    this.unloadAndCloseAuthWindow(token, expires)
  }
}