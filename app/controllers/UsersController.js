const User = require('../models/User')
module.exports = class UsersController {
  constructor () {
    this._quota_max = 10000
    this._quota_remaining = 10000
    this._display_name = ''
    this._profile_image = ''
    this._link = ''
    this._website_url = ''
    this._location = ''
    this._user_id = ''
    this._user_type = ''
    this._creation_date = ''
    this._reputation = ''
    this._account_id = ''
    this._is_employee = ''
    this._badge_counts = 0
  }

  me () {
    return User.me().then((res) => {
      const profileInformation = res['items'][0]
      this._creation_date = profileInformation['creation_date']
      this._website_url = profileInformation['website_url']
      this._user_id = profileInformation['user_id']
      this._user_type = profileInformation['user_type']
      this._account_id = profileInformation['account_id']
      this._is_employee = profileInformation['is_employee']
      this._reputation = profileInformation['reputation']
      this._location = profileInformation['location'].unescapeHTML()
      this._link = profileInformation['link']
      this._profile_image = profileInformation['profile_image']
      this._display_name = profileInformation['display_name'].unescapeHTML()
    })
  }
}