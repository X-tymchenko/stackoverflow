const Session = require('../models/Session')
let instance = null
module.exports = class SessionsController {
  constructor () {
    if (!instance) {
      instance = this
    }
    const {token, expires, created_at} = Session.load()
    this.token = token
    this.expires = expires
    this.created_at = created_at
    this.timeOut = Session.verify_validity(this)
    return instance
  }

  static create (token, expires, created_at) {
    this.token = token
    this.expires = expires
    this.created_at = created_at
    Session.save(this)
  }

  static destroy () {
    this.token = null
    this.expires = null
    this.created_at = null
  }

}
