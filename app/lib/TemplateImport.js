module.exports = class TemplateImport {
  static load (name, append_to) {
    if (!name)
      return
    const link = document.querySelector(`#${name}`)
    let template = link.import.querySelector(`.${name}`)
    let clone = document.importNode(template.content, true)
    return $(append_to).append(clone)
  }
}