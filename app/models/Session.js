// const {ipcMain} = require('electron')
const moment = require('moment')
module.exports = class Session {
  constructor () {
    this.token = 0
    this.expires = 0
    this.created_at = null
  }

  static verify_validity (cashed_session) {
    const {expires, created_at} = cashed_session
    return !!(moment(created_at).unix() + Number(expires) >
      moment(new Date()).unix())
  }

  static save (cashed_session) {
    const {expires, token, created_at} = cashed_session
    localStorage.setItem('token', token)
    localStorage.setItem('expires', expires)
    localStorage.setItem('created_at', created_at)
  }

  static load () {
    this.token = localStorage.getItem('token')
    this.expires = localStorage.getItem('expires')
    this.created_at = localStorage.getItem('created_at') || new Date()
    return this
  }

  static destroy () {
    localStorage.removeItem('token')
    localStorage.removeItem('expires')
    localStorage.removeItem('created_at')
  }
}
