const RequestBuilder = require('../lib/RequestBuilder')

module.exports = class User {

  constructor () {

  }

  static keys () {
    return {
      quota_max: 'quota_max',
      quota_remaining: 'quota_remaining',
      display_name: 'display_name',
      profile_image: 'profile_image',
      link: 'link',
      website_url: 'website_url',
      location: 'location',
      user_id: 'user_id',
      user_type: 'user_type',
      creation_date: 'creation_date',
      reputation: 'reputation',
      account_id: 'account_id',
      is_employee: 'is_employee',
      badge_counts: 'badge_counts',
    }
  }

  /* TODO:
  * load current_user_
  */
  static me () {
    const url = 'me'
    return RequestBuilder.fetch(url, {})
  }

  /* TODO:
  * find user information by id
  */
  find (id) {}

  /* TODO
  * update current user information
  */
  update () {}
}