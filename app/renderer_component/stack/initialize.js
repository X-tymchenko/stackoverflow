const {ipcRenderer} = require('electron')
const UserController = require('../../controllers/UsersController')
const header = require('../../views/main')

ipcRenderer.on('stack:init-page', () => {
  let user = new UserController()

  user.me().then(() => {
    const template = header(user)
    $(document.body).append(template)
  })

})