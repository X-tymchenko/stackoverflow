const {ipcRenderer} = require('electron')
const SessionsController = require('../../controllers/SessionsController')
ipcRenderer.send('stack:login', new SessionsController())