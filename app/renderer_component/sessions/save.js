const {ipcRenderer} = require('electron')
const SessionsController = require('../../controllers/SessionsController')

ipcRenderer.on('stack:session-save', (event, obj) => {
  SessionsController.create(obj.token, obj.expires, obj.created_at)
})