const {app, ipcMain, BrowserWindow} = require('electron')
const LoginWindow = require('./app/main_components/login_window')
const path = require('path')
const url = require('url')
let win

const createWindow = () => {
  win = new BrowserWindow({
    width: 600, height: 600,
    title: 'Stack Overflow',
  })
  win.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:', slashes: true,
  }))

  win.on('closed', () => win = null)
}
app.on('ready', () => createWindow())

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (win === null) {
    createWindow()
  }
})

ipcMain.on('stack:login', (event, session) => {
  const {timeOut, token, expires, created_at} = session
  if (timeOut && (token && expires && created_at)) {
    win.webContents.send('stack:init-page', session)
    return
  }
  let loginWindow = new LoginWindow({
    width: 800, height: 600,
    show: false, alwaysOnTop: true,
    webPreferences: {nodeIntegration: false},
  }, function (token, expires, created_at) {
    win.webContents.send('stack:session-save', {token, expires, created_at})
    win.webContents.send('stack:init-page', session)
  })
  loginWindow.loadAuthUrl()
})